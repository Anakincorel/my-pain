import posts from './posts';
import user from './user';

const state = Object.assign(
  {
    error: {
      title: null,
      message: null,
      error: null
    }
  },
  posts,
  user
);

export default state;
