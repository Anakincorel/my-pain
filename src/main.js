// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import store from './vuex';

import AdHeader from './components/header/AdHeader';
import AdSidebar from './components/sidebar/AdSidebar';
import AdButton from './components/ui/AdButton';

Vue.config.productionTip = false;

Vue.component('ad-header', AdHeader);
Vue.component('ad-sidebar', AdSidebar);
Vue.component('ad-button', AdButton);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>'
});
